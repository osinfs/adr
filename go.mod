module adr

go 1.16

require (
	github.com/gosimple/slug v1.12.0
	github.com/spf13/afero v1.8.0 // indirect
	github.com/spf13/cobra v1.3.0
	github.com/spf13/viper v1.10.1
	golang.org/x/sys v0.0.0-20220128215802-99c3d69c2c27 // indirect
	gopkg.in/ini.v1 v1.66.3 // indirect
)
