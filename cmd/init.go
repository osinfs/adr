/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"io/fs"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// initCmd represents the init command
var initCmd = &cobra.Command{
	Use:   "init",
	Short: "Initialize the subdirectory to file Architectural Decision Records",
	Long: `Initialize the subdirectory to file Architectural Decision Records

- creates the subdirectory to file ADR (default: ./doc/adr)
- creates adr config file in the current subdirectory
- creates the first ADR in that subdirectory, recording the decision to
  record architectural decisions with ADR.`,
	Run: func(cmd *cobra.Command, args []string) {
		directory, _ := cmd.Flags().GetString("directory")
		err := adrInitialize(directory)
		if err != nil {
			cobra.CheckErr(err)
		}
	},
}

func init() {
	rootCmd.AddCommand(initCmd)

	// flag for subdirectory to file ADR on
	initCmd.Flags().StringP("directory", "d", "doc/adr", "Subdirectory to file ADR on")
}

func adrInitialize(path string) error {
	var err error

	viper.Set("directory", path)
	viper.Set("dateFormat", "2006/01/02")
	err = viper.SafeWriteConfig()
	if err != nil {
		return err
	}

	err = os.MkdirAll(path, fs.ModePerm)
	if err != nil {
		return err
	}

	adrAdd("Record architecture decisions", -1)

	return nil
}
