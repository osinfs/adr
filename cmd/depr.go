/*
Copyright © 2022 Adriano Vieira <adriano.svieira@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"adr/cmd/helpers"
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// deprCmd represents the depr command
var deprCmd = &cobra.Command{
	Use:   "depr <adr_number>",
	Short: "Change the ADR to deprecated",
	Long: `Change the ADR to deprecated.

To accomplish that the ADR needs:
- has the status: "Proposed" or "Accepted"

For example:
  $ adr depr 000010`,
	Args: cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		if helpers.CheckInitialized() {
			cobra.CheckErr("not initialized")
		}

		if len(args) != 1 {
			cmd.Help()
			os.Exit(0)
		}

		n, err := strconv.Atoi(args[0])
		if err != nil {
			cobra.CheckErr("Invalid ADR number")
		} else if !(n > 0) {
			cobra.CheckErr("Invalid ADR number")
		}

		if err := deprecate(n); err != nil {
			cobra.CheckErr(err)
		}

	},
}

func init() {
	rootCmd.AddCommand(deprCmd)
}

func deprecate(adrNumber int) error {
	var fileName string = ""
	directory := viper.GetString("directory")
	sID := fmt.Sprintf("adr-%06d", adrNumber)
	files, _ := os.ReadDir(directory)
	for _, f := range files {
		if strings.HasPrefix(f.Name(), sID) {
			fileName = f.Name()
			break
		}
	}

	if fileName == "" {
		return fmt.Errorf("ADR %s not found", sID)
	}

	file, err := os.OpenFile(directory+"/"+fileName, os.O_RDWR, os.ModePerm)
	if err != nil {
		return err
	}
	defer file.Close()

	fileScanner := bufio.NewScanner(file)

	fileScanner.Split(bufio.ScanLines)

	var fileLines string = ""
	var fileLine string = ""
	var isDeprecatable bool = false
	for fileScanner.Scan() {
		fileLine = fileScanner.Text() + "\n"
		if strings.Compare(fileScanner.Text(), "Proposed") == 0 || strings.Compare(fileScanner.Text(), "Accepted") == 0 {
			fileLine = fmt.Sprintln("Deprecated")
			isDeprecatable = true
		}
		fileLines += fileLine
	}

	if isDeprecatable {
		if err := file.Truncate(0); err != nil {
			return fmt.Errorf("error trying to rewrite %s", sID)
		}
		file.Seek(0, 0)
		_, err = file.Write([]byte(fileLines))
		if err != nil {
			return fmt.Errorf("error trying to update status of %s", sID)
		}
		fmt.Printf("ADR %s signed as deprecated\n", sID)
	} else {
		fmt.Printf("%v is not elegible to be deprecated\n", sID)
	}

	return nil
}
