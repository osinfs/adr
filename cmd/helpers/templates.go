package helpers

type Record struct {
	Id            string
	Title         string
	Status        string
	Date          string
	Superseds     string
	SupersedsSlug string
}

const initialRecord = `
= {{.Id}}. {{.Title}}

Date: {{.Date}}

== Status

{{.Status}}

== Context

We need to record the architectural decisions made on this project.

== Decision

We will use Architecture Decision Records, as described by
Michael Nygard article footnote:[Michael Nygard article: http://thinkrelevance.com/blog/2011/11/15/documenting-architecture-decisions[]]

== Consequences

See Michael Nygard's article reference in footnotes.

== References
`

const standardRecord = `
= {{.Id}}. {{.Title}}

Date: {{.Date}}

== Status

{{.Status}}

{{ if .Superseds -}}
* Superseds link:{{.SupersedsSlug}}[{{.Superseds}}]

{{ end -}}

== Context

The issue motivating this decision, and any context that influences or constrains the decision.

== Decision

The change that we're proposing or have agreed to implement.

== Consequences

What becomes easier or more difficult to do and any risks introduced by the change that will need to be mitigated.

== References

`

func AdocRecord(initial bool) string {
	if initial {
		return initialRecord
	}
	return standardRecord
}
