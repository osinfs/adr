package helpers

import (
	"fmt"
	"os"

	"github.com/spf13/viper"
)

func GetSuperseded(superseded int) string {
	var superseds string = ""
	if superseded > 0 {
		superseds = fmt.Sprintf(`%06d - %s`, superseded, superseds)
	}
	return superseds
}

func CheckInitialized() bool {
	return !viper.InConfig("directory")
}

func GetSeqAdrId() int {
	directory, _ := os.ReadDir(viper.GetString("directory"))
	return len(directory)
}
