/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"adr/cmd/helpers"
	"errors"
	"fmt"
	"os"
	"strings"
	"text/template"
	"time"

	"github.com/gosimple/slug"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// addCmd represents the add command
var addCmd = &cobra.Command{
	Use:   "add <'the adr title'>",
	Short: "Add Architectural Decision Record",
	Long: `Add Architectural Decision Record

- creates ADRs in subdirectory`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) != 1 {
			cmd.Help()
			os.Exit(0)
		}
		superseded, _ := cmd.Flags().GetInt("supersed")

		err := adrAdd(args[0], superseded)
		if err != nil {
			cobra.CheckErr(err)
		}
	},
}

func init() {
	rootCmd.AddCommand(addCmd)

	addCmd.Flags().IntP("supersed", "s", 0, "Identify ADR to be superseded by the new one (e.g.: 000001)")
}

func adrAdd(title string, superseds int) error {
	var status string = "proposed"

	if superseds == -1 {
		status = "accepted"
	} else if helpers.CheckInitialized() {
		return errors.New("not initialized")
	}

	t := time.Now().UTC()
	date := t.Local().Format(viper.GetString("dateFormat"))

	titleSlug := slug.Make(title)
	directory := viper.GetString("directory")
	adrId := helpers.GetSeqAdrId() + 1

	adrFile, err := os.Create(fmt.Sprintf("%s/adr-%06d-%s.adoc", directory, adrId, titleSlug))
	if err != nil {
		return err
	}
	defer adrFile.Close()

	tmpl, err := template.New("adr").Parse(helpers.AdocRecord(superseds == -1))
	if err != nil {
		return err
	}
	err = tmpl.Execute(adrFile, helpers.Record{
		Id:            fmt.Sprintf("%06d", adrId),
		Title:         strings.Title(title),
		Status:        strings.Title(status),
		Date:          date,
		Superseds:     helpers.GetSuperseded(superseds),
		SupersedsSlug: slug.Make(helpers.GetSuperseded(superseds)),
	})

	if err != nil {
		return err
	}
	fmt.Printf(`ADR "%s" filed at "%s" directory.`+"\n", title, viper.GetString("directory"))
	return nil
}
