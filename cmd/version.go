/*
Copyright © 2022 Adriano Vieira adriano.svieira@gmail.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"os"
	"text/template"

	"github.com/spf13/cobra"
)

// versionCmd represents the version command
var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "show version information",
	Long:  `Show application version informations.`,
	Run: func(cmd *cobra.Command, args []string) {
		showVersionDetails, _ := cmd.Flags().GetBool("verbose")

		if err := showVersion(showVersionDetails); err != nil {
			cobra.CheckErr(err)
		}
	},
}

type versionDetail struct {
	Version   string
	GoVersion string
	BuiltAt   string
}

const (
	// app version, e.g. 0.1.0-alpha1
	version = "ADR_VERSION"
	// go version, e.g. go1.16.3 linux/amd64
	goVersion = "GO_VERSION"
	// build date, e.g. 2022-01-13 20:15:33
	builtAt = "BUILDDATETIME"
)

func init() {
	rootCmd.AddCommand(versionCmd)

	// To show version details
	versionCmd.Flags().BoolP("verbose", "v", false, "show version details")
}

type Inventory struct {
	Material string
	Count    uint
}

// version show version details
func showVersion(showVersionDetails bool) error {
	const versionDetailsTemplate = `adr tool version details.
adr version: {{ .Version }}
 Go version: {{ .GoVersion }}
   Built at: {{ .BuiltAt }}
`

	tmpl, err := template.New("version").Parse(versionDetailsTemplate)
	if err != nil {
		return err
	}

	if showVersionDetails {
		tmpl.Execute(os.Stdout, versionDetail{version, goVersion, builtAt})
	} else {
		fmt.Printf("adr version: %v\n", version)
	}

	return nil
}
